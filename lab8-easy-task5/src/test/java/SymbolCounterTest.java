import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SymbolCounterTest {

    @Test
    public void countTest() {
        Assertions.assertEquals(3, SymbolCounter.count("abcdeabcadf", 'a'));
        Assertions.assertEquals(0, SymbolCounter.count("fff", 'a'));
        Assertions.assertEquals(0, SymbolCounter.count("", 'a'));
        Assertions.assertEquals(0, SymbolCounter.count(null, 'a'));
    }

}
