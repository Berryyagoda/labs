public class SymbolCounter {

    public static int count(String input, char symbol) {
        if(input == null)
            return 0;
        int result = 0;
        for(int i = 0; i < input.length(); i++) {
            if(input.charAt(i) == symbol)
                result++;
        }
        return result;
    }

}
