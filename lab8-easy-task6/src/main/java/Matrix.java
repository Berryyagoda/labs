public class Matrix {

    private final int[][] content;

    public Matrix(int[][] matrix) {
        content = matrix;
    }

    public void printUnderDiagonal() {
        for (int i = 1; i < content.length; i++) {
            for (int j = 0; j < i; j++) {
                System.out.print(content[i][j] + " ");
            }
            System.out.println();
        }
    }

}
