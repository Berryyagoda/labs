public class Matrix {

    private final double[][] content;

    public Matrix(double[][] data) {
        content = data;
    }

    public void print() {
        for(int i = 0; i < content.length; i++) {
            for(int k = 0; k < content[0].length; k++) {
                System.out.printf("%.2f ", content[i][k]);
            }
            System.out.println();
        }
    }

    public Matrix add(Matrix other) {
        if(content.length != other.content.length || content[0].length != other.content[0].length)
            throw new RuntimeException("Размерность матриц должна быть одинаковой");
        double[][] result = new double[content.length][content[0].length];
        for(int i = 0; i < content.length; i++) {
            for(int k = 0; k < content[0].length; k++) {
                result[i][k] = content[i][k] + other.content[i][k];
            }
        }
        return new Matrix(result);
    }

    public Matrix subtract(Matrix other) {
        if(content.length != other.content.length || content[0].length != other.content[0].length)
            throw new RuntimeException("Размерность матриц должна быть одинаковой");
        double[][] result = new double[content.length][content[0].length];
        for(int i = 0; i < content.length; i++) {
            for(int k = 0; k < content[0].length; k++) {
                result[i][k] = content[i][k] - other.content[i][k];
            }
        }
        return new Matrix(result);
    }

    public Matrix multiply(Matrix other) {
        if(content[0].length != other.content.length)
            throw new RuntimeException("Размерность матриц не подходит для умножения");
        double[][] result = new double[content.length][other.content[0].length];
        for(int i = 0; i < content.length; i++) {
            for(int k = 0; k < other.content[0].length; k++) {
                for(int j = 0; j < other.content.length; j++) {
                    result[i][k] += content[i][j] * other.content[j][k];
                }
            }
        }
        return new Matrix(result);
    }

    public Matrix multiply(double modifier) {
        double[][] result = new double[content.length][content[0].length];
        for(int i = 0; i < content.length; i++) {
            for(int k = 0; k < content[0].length; k++) {
                result[i][k] = content[i][k] * modifier;
            }
        }
        return new Matrix(result);
    }

    public boolean equals(double[][] matrix) {
        for(int i = 0; i < content.length; i++) {
            for(int k = 0; k < content[0].length; k++) {
                if(content[i][k] != matrix[i][k]) return false;
            }
        }
        return true;
    }

}
