public class Application {

    public static void main(String[] args) {
        Matrix source = new Matrix(new double[][] {
                {1, -1, 1},
                {-1, 1, -1}
        });
        Matrix second = new Matrix(new double[][] {
                {-1, 1, -1},
                {1, -1, 1}
        });
        source.add(second).print();
        System.out.println("-----");
        source.subtract(second).print();
        System.out.println("-----");
        source = new Matrix(new double[][] {
                {1, 2},
                {3, 4},
                {5, 6}
        });
        second = new Matrix(new double[][] {
                {2, 1, 1},
                {0.5, 1, 0.5}
        });
        source.multiply(second).print();
        System.out.println("-----");
        source.multiply(5).print();
    }

}
