import org.junit.jupiter.api.Test;

public class MatrixTest {

    @Test
    public void additionTest() {
        Matrix source = new Matrix(new double[][] {
                {1, -1, 1},
                {-1, 1, -1}
        });
        Matrix second = new Matrix(new double[][] {
                {-1, 1, -1},
                {1, -1, 1}
        });
        assert source.add(second).equals(new double[][] {
                {0, 0, 0},
                {0, 0, 0}
        });
    }

    @Test
    public void subtractionTest() {
        Matrix source = new Matrix(new double[][] {
                {1, -1, 1},
                {-1, 1, -1}
        });
        Matrix second = new Matrix(new double[][] {
                {-1, 1, -1},
                {1, -1, 1}
        });
        assert source.subtract(second).equals(new double[][] {
                {2, -2, 2},
                {-2, 2, -2}
        });
    }

    @Test
    public void multiplicationTest() {
        Matrix source = new Matrix(new double[][] {
                {1, 2},
                {3, 4},
                {5, 6}
        });
        Matrix second = new Matrix(new double[][] {
                {2, 1, 1},
                {0.5, 1, 0.5}
        });
        Matrix result = source.multiply(second);
        assert result.equals(new double[][] {
                {3, 3, 2},
                {8, 7, 5},
                {13, 11, 8}
        });
    }

    @Test
    public void multiplicationModifierTest() {
        Matrix source = new Matrix(new double[][] {
                {1, 2},
                {3, 4},
                {5, 6}
        });
        Matrix result = source.multiply(3);
        assert result.equals(new double[][] {
                {3, 6},
                {9, 12},
                {15, 18}
        });
    }

}
