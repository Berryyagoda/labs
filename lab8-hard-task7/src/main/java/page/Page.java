package page;

import action.Action;

import java.util.Scanner;

public class Page {

    private String backMessage;
    private String pageTitle;
    private String description;
    public Page parent;
    public Page[] childes;
    public Action[] actions;

    public Page(String backMessage, String pageTitle, String description, Page parent, Page[] childes, Action... actions) {
        this.backMessage = backMessage;
        this.pageTitle = pageTitle;
        this.description = description;
        this.parent = parent;
        this.childes = childes;
        this.actions = actions;
    }

    public String getBackMessage() {
        return backMessage;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public String getDescription() {
        return description;
    }
}
