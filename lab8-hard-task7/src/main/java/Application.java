import action.Action;
import action.PrintTextAction;
import page.Page;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        Page first = new Page(
                "Выйти из личного кабинета",
                "",
                "Главное меню\nВыберите действие:",
                null,
                new Page[1],
                new PrintTextAction("Вывести ФИО студента", 2, 0, "Ваше ФИО:\nИванов Иван Иванович"),
                new PrintTextAction("Вывести группу студента", 3, 0, "Ваша группа: 21АА1")
        );
        Page second = new Page(
                "Вернуться в главное меню",
                "Зачетная книжка",
                "Выберите дальнейшее действие:",
                first,
                new Page[1],
                new PrintTextAction("Вывести номер семестра", 2, 0, "Текущий семестр: 3")
        );
        first.childes[0] = second;
        Page third = new Page(
                "Вернуться к зачетной книжке",
                "Посмотреть оценки за прошлую сессию",
                "Выберите предмет:",
                second,
                new Page[0],
                new PrintTextAction("Математика", 1, 0, "Математика: 5"),
                new PrintTextAction("История", 2, 0, "История: 4")
        );
        second.childes[0] = third;
        Page currentPage = first;
        Scanner scanner = new Scanner(System.in);
        printPageInfo(currentPage);
        int actionNum = scanner.nextInt();
        while(true) {
            if(actionNum == 0) {
                if(currentPage.parent == null)
                    System.exit(0);
                else
                    currentPage = currentPage.parent;
            } else {
                if(actionNum - 1 < currentPage.childes.length) {
                    currentPage = currentPage.childes[actionNum - 1];
                } else {
                    if(actionNum - 1 - currentPage.childes.length < currentPage.actions.length) {
                        Action action = currentPage.actions[actionNum - 1 - currentPage.childes.length];
                        String[] actionArgs = new String[action.getArgsAmount()];
                        scanner.nextLine();
                        for(int i = 0; i < actionArgs.length; i++)
                            actionArgs[i] = scanner.nextLine();
                        action.run(actionArgs);
                    } else {
                        System.out.println("Неизвестное действие");
                    }
                }
            }
            printPageInfo(currentPage);
            actionNum = scanner.nextInt();
        }
    }

    private static void printPageInfo(Page page) {
        System.out.println(page.getDescription());
        System.out.println("0 - " + page.getBackMessage());
        int i = 1;
        for(Page child : page.childes) {
            System.out.println(i + " - " + child.getPageTitle());
            i++;
        }
        for(Action action : page.actions) {
            System.out.println(i + " - " + action.getName());
            i++;
        }
    }

}
