package action;

public abstract class Action {

    private String name;
    private int index;
    private int argsAmount;

    public Action(String name, int index, int argsAmount) {
        this.name = name;
        this.index = index;
        this.argsAmount = argsAmount;
    }

    public String getName() {
        return name;
    }

    public int getIndex() {
        return index;
    }

    public int getArgsAmount() {
        return argsAmount;
    }

    public abstract void run(String... args);

}
