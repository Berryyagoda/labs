package action;

public class PrintTextAction extends Action {

    private String text;

    public PrintTextAction(String name, int index, int argsAmount, String text) {
        super(name, index, argsAmount);
        this.text = text;
    }

    @Override
    public void run(String... args) {
        System.out.println(text);
    }
}
