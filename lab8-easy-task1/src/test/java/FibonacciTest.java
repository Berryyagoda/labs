import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class FibonacciTest {

    @Test
    public void fibonacciTest() {
        assert Arrays.equals(Fibonacci.fibonacci(7), new long[] {0, 1, 1, 2, 3, 5, 8});
        assert Arrays.equals(Fibonacci.fibonacci(10), new long[] {0, 1, 1, 2, 3, 5, 8, 13, 21, 34});
    }

    @Test
    public void fibonacciRecursiveTest() {
        assert Arrays.equals(Fibonacci.fibonacciRecursive(7), new long[] {0, 1, 1, 2, 3, 5, 8});
        assert Arrays.equals(Fibonacci.fibonacciRecursive(10), new long[] {0, 1, 1, 2, 3, 5, 8, 13, 21, 34});
    }

}
