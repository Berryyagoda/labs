import java.util.Arrays;

public class Fibonacci {

    public static long[] fibonacci(int n) {
        long[] result = new long[n];
        result[0] = 0;
        result[1] = 1;
        for (int i = 2; i < n; i++) {
            result[i] = result[i-1] + result[i-2];
        }
        return result;
    }

    public static long[] fibonacciRecursive(int n) {
        return fibonacciRecursive(n-1, new long[n]);
    }

    private static long[] fibonacciRecursive(int n, long[] arr) {
        if (n == 0) {
            arr[0] = 0;
            return arr;
        }
        if (n == 1) {
            arr[0] = 0;
            arr[1] = 1;
            return arr;
        }
        arr = fibonacciRecursive(n - 1, arr);
        arr[n] = arr[n-1] + arr[n - 2];
        return arr;
    }

}
