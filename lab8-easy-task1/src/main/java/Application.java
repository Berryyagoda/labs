import java.util.Arrays;

public class Application {

    public static void main(String[] args) {
        System.out.println("Fibonacci(5): " + Arrays.toString(Fibonacci.fibonacci(5)));
        System.out.println("Fibonacci(5) recursive: " + Arrays.toString(Fibonacci.fibonacciRecursive(5)));
    }

}