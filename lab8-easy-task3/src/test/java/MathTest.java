import org.junit.jupiter.api.Test;

public class MathTest {

    @Test
    public void naturalNumbersSumTest() {
        long result = MathUtils.naturalNumberSum(3);
        assert result == 6;
        result = MathUtils.naturalNumberSum(6);
        assert result == 21;
    }

}
