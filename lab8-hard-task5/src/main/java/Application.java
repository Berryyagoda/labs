import java.util.Random;
import java.util.Scanner;

public class Application {

    private static Random random = new Random();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double repeats = scanner.nextDouble();
        double minValue = scanner.nextDouble();
        double maxValue = scanner.nextDouble();
        double offset = scanner.nextDouble();
        minValue += offset;
        maxValue += offset;
        int intersections = 0;
        for(int i = 0; i < repeats; i++) {
            double start1 = rand(minValue, maxValue);
            double end1 = rand(start1, maxValue);
            double start2 = rand(minValue, maxValue);
            double end2 = rand(start2, maxValue);
            if(start1 >= start2 && start1 <= end2 || end1 >= start2 && end1 <= end2 || start2 >= start1 && start2 <= end1 || end2 >= start1 && end2 <= end1) {
                intersections++;
            }
        }
        System.out.println(intersections);
    }

    private static double rand(double min, double max) {
        return min + (max - min) * random.nextDouble();
    }

}
