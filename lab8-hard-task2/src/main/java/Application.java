import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        printMenu();
        int action = scanner.nextInt();
        while(action == 1) {
            System.out.print("Введите число: ");
            int amount = scanner.nextInt();
            System.out.println("Результат:");
            NumberGenerator.generate(amount);
            printMenu();
            action = scanner.nextInt();
        }
    }

    private static void printMenu() {
        System.out.println("Выберите действие: ");
        System.out.println("1) Ввести число");
        System.out.println("0) Завершить программу");
        System.out.print("Ваш выбор: ");
    }

}
