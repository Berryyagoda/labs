import java.util.Arrays;
import java.util.Locale;

public class NumberGenerator {

    private static final long TIMEOUT = 5000;

    public static void generate(int amount) {
        long startTime = System.currentTimeMillis();
        double[] array = new double[amount];
        for(int i = 0; i < amount; i++)
            array[i] = Math.random();
        long endTime = System.currentTimeMillis();
        if(endTime - startTime < TIMEOUT) {
            printArray(array);
            System.out.println();
        } else {
            System.out.println("Превышено допустимое время работы программы, введите количество поменьше");
        }
    }

    private static void printArray(double[] array) {
        if(array.length > 100) {
            printArray(Arrays.copyOfRange(array, 0, 10));
            System.out.print(" - " + (array.length - 20) + " more numbers - ");
            printArray(Arrays.copyOfRange(array, array.length - 10, array.length));
        } else {
            for (int i = 0; i < array.length; i++) {
                System.out.printf(Locale.ENGLISH, "%.2f", array[i]);
                if (i + 1 != array.length)
                    System.out.print(", ");
            }
        }
    }

}
