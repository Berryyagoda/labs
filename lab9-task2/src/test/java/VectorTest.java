import org.junit.jupiter.api.Test;

public class VectorTest {

    @Test
    public void additionTest() {
        Vector vector = new Vector(1, 5);
        Vector target = new Vector(2, 9);
        assert vector.sum(target).equals(new Vector(3, 14));
    }

    @Test
    public void subtractionTest() {
        Vector vector = new Vector(1, 5);
        Vector target = new Vector(2, 9);
        assert vector.subtract(target).equals(new Vector(-1, -4));
    }

    @Test
    public void multiplicationTest() {
        Vector vector = new Vector(3, 5);
        assert vector.multiply(3).equals(new Vector(9, 15));
    }

    @Test
    public void divisionTest() {
        Vector vector = new Vector(4, 2);
        assert vector.division(2).equals(new Vector(2, 1));
    }

}
