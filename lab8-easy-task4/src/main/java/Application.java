import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите количество строк: ");
        int rows = scanner.nextInt();
        System.out.print("Введите количество столбцов: ");
        int columns = scanner.nextInt();
        System.out.println("Заполните матрицу:");
        int[][] matrix = new int[rows][columns];
        System.out.println(matrix.length);
        for(int i = 0; i < rows; i++) {
            for(int k = 0; k < columns; k++) {
                System.out.print("matrix[" + i + "][" + k + "] = ");
                matrix[i][k] = scanner.nextInt();
            }
        }
        System.out.println("Введенная матрица:");
        print(matrix);
        int[][] transposed = new int[columns][rows];
        for (int i = 0; i < columns; i++) {
            for (int j = 0; j < rows; j++) {
                transposed[i][j] = matrix[j][i];
            }
        }
        System.out.println("Транспонированная матрица:");
        print(transposed);
    }

    private static void print(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

}
