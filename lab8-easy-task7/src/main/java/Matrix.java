public class Matrix {

    private final int[][] content;

    public Matrix(int[][] matrix) {
        content = matrix;
    }

    public void print() {
        for (int i = 0; i < content.length; i++) {
            for (int j = 0; j < content[0].length; j++) {
                System.out.print(content[i][j] + " ");
            }
        }
    }

}
