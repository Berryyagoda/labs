public class Application {

    public static void main(String[] args) {
        Matrix matrix = new Matrix(new int[][] {
                {1, 2, 3},
                {4, 5, 6}
        });
        matrix.print();
    }

}
