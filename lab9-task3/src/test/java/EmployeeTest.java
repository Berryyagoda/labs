import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class EmployeeTest {

    @Test
    public void employeeTest() {
        Employee employee = new Engineer("Ivan", "Trainee", 10000);
        Assertions.assertEquals("Ivan", employee.getName());
        Assertions.assertEquals("Trainee", employee.getPosition());
        Assertions.assertEquals(10000, employee.getSalary());
    }

    @Test
    public void engineerTest() {
        Employee employee = new Engineer("Ivan", "Trainee", 10000);
        Assertions.assertEquals("Работа заключается в производстве деталей на заводе", employee.getDescription());
    }

    @Test
    public void workerTest() {
        Employee employee = new Worker("Ivan", "Trainee", 10000);
        Assertions.assertEquals("Занимается тяжелым физическим трудом", employee.getDescription());
    }

    @Test
    public void accountantTest() {
        Employee employee = new Accountant("Ivan", "Trainee", 10000);
        Assertions.assertEquals("Работа заключается в подсчете и выплате зарплаты", employee.getDescription());
    }

    @Test
    public void generalAccountantTest() {
        GeneralAccountant employee = new GeneralAccountant("Ivan", "Trainee", 10000);
        Assertions.assertEquals("Занимается установкой зарплаты", employee.getDescription());
        Employee target = new Engineer("Sergey", "Engineer", 20000);
        employee.setNewSalary(target, 40000);
        Assertions.assertEquals(40000, target.getSalary());
    }

    @Test
    public void equalsTest() {
        Employee first = new GeneralAccountant("Ivan", "Trainee", 10000);
        Employee second = new GeneralAccountant("Ivan", "Trainee", 10000);
        Assertions.assertEquals(first, second);
    }

    @Test
    public void equalsFailTest() {
        Employee first = new GeneralAccountant("Ivan", "Trainee", 10000);
        Employee second = new Engineer("Sergey", "Engineer", 20000);
        Assertions.assertNotEquals(first, second);
    }

    @Test
    public void hashCodeTest() {
        Employee employee = new Engineer("Sergey", "Engineer", 20000);
        Assertions.assertEquals(20680, employee.hashCode());
    }

}
