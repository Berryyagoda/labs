public abstract class Employee {
    
    private final String name;
    private String position;
    private int salary;

    public Employee(String name, String position, int salary) {
        this.name = name;
        this.position = position;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public String getPosition() {
        return position;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public abstract String getDescription();

    @Override
    public String toString() {
        return "\nИмя: " + name + "\nДолжность: " + position + "\nЗарплата: " + salary + "\nОписание: " + getDescription();
    }

    public int hashCode() {
        return name.length() * 100 + position.length() * 10 + salary;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Employee) {
            return this.name.equals(((Employee) obj).getName()) &&
                    this.position.equals(((Employee) obj).getPosition()) &&
                    this.salary == ((Employee) obj).getSalary() &&
                    getDescription().equals(((Employee) obj).getDescription());
        }
        return false;
    }

    @Override
    public Object clone(){
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }


}
