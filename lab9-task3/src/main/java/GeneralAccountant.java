import java.util.Objects;

public class GeneralAccountant extends Employee {

    public GeneralAccountant(String name, String position, int salary) {
        super(name, position, salary);
    }

    @Override
    public String getDescription() {
        return "Занимается установкой зарплаты";
    }

    public void setNewSalary(Employee employee, int salary) {
        employee.setSalary(salary);
        System.out.println("Сотруднику " + employee.getName() + " установлена зарплата в размере " + salary);
    }

}
