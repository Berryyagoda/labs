import java.util.Objects;
import java.util.Random;

public class Engineer extends Employee {

    public Engineer(String name, String position, int salary) {
        super(name, position, salary);
    }

    @Override
    public String getDescription() {
        return "Работа заключается в производстве деталей на заводе";
    }

    public void createDetail() {
        Random random = new Random();
        if(random.nextBoolean())
            System.out.println("Была произведена деталь без брака");
        else
            System.out.println("Была произведена бракованная деталь");
    }

}
