public class Application {

    public static void main(String[] args) {
        Employee[] employees = new Employee[] {
                new Engineer("Ivan", "Ученик инженера", 10000),
                new Accountant("Sergey", "Стажер", 15000),
                new GeneralAccountant("Victor", "Главбух", 90000),
                new Worker("Andrew", "Грузчик", 35000)
        };
        for (Employee employee : employees)
            System.out.println(employee);
    }

}
