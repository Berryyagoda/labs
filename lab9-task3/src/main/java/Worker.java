public class Worker extends Employee {

    public Worker(String name, String position, int salary) {
        super(name, position, salary);
    }

    @Override
    public String getDescription() {
        return "Занимается тяжелым физическим трудом";
    }

    public void unloadGoods() {
        System.out.println("Товар успешно разгружен");
    }

}
