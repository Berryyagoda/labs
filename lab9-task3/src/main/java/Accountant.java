import java.util.Objects;

public class Accountant extends Employee {

    public Accountant(String name, String position, int salary) {
        super(name, position, salary);
    }

    @Override
    public String getDescription() {
        return "Работа заключается в подсчете и выплате зарплаты";
    }

    public void paySalary(Employee employee) {
        System.out.println("Сотруднику " + employee.getName() + " была начислина зарплата в размере " + employee.getSalary());
    }

}
