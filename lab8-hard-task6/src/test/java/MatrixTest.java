import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MatrixTest {

    @Test
    public void multiplyTest() {
        double[][] first = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        double[][] second = {
                {9, 8, 7},
                {6, 5, 4},
                {3, 2, 1}
        };
        double[][] multiplication = Application.multiply(first, second);
        double[][] excepted = {
                {30, 24, 18},
                {84, 69, 54},
                {138, 114, 90}
        };
        Assertions.assertArrayEquals(excepted, multiplication);
    }

    @Test
    public void exceptionTest() {
        double[][] first = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        double[][] second = {
                {9, 8, 7},
                {6, 5, 4}
        };
        Assertions.assertThrows(IllegalArgumentException.class, () -> Application.multiply(first, second));
    }

}
