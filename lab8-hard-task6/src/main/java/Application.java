import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите количество матриц: ");
        int amount = scanner.nextInt();
        System.out.print("Введите размерность матриц: ");
        int n = scanner.nextInt();
        System.out.println("Полученные матрицы:");
        double[][][] matrixArray = generate(amount, n, n);
        for (int i = 0; i < amount; i++) {
            print(matrixArray[i]);
        }


        for (int i = 0; i < amount - 1; i++) {
            for (int l = i + 1; l < amount; l++) {
                System.out.print("matrix#" + (i+1) + " x matrix#" + (l+1) + " time = ");
                multiply(matrixArray[i], matrixArray[l]);
            }
        }

    }

    private static void print(double[][] matrix) {
        for (int l = 0; l < matrix.length; l++) {
            for (int k = 0; k < matrix[0].length; k++) {
                System.out.printf("%.3f ", matrix[l][k]);
            }
            System.out.println();
        }
        System.out.println("------------");
    }

    public static double[][][] generate(int amount, int n, int m) {
        double[][][] matrix = new double[amount][n][m];
        for (int i = 0; i < amount; i++) {
            for (int l = 0; l < n; l++) {
                for (int k = 0; k < m; k++) {
                    matrix[i][l][k] = Math.random() * 10;
                }
            }
        }
        return matrix;
    }

    public static double[][] multiply(double[][] matrix1, double[][] matrix2) {
        if(matrix1.length != matrix1[0].length || matrix2.length != matrix2[0].length || matrix1.length != matrix2.length)
            throw new IllegalArgumentException();
        double[][] result_array = new double[matrix1.length][matrix2[0].length];
        long ns = System.nanoTime();
        for (int i = 0; i < matrix1[0].length; i++) {
            for (int l = 0; l < matrix2.length; l++) {
                for (int y = 0; y < matrix2[0].length; y++) {
                    result_array[i][l] = result_array[i][l] + matrix1[i][y] * matrix2[y][l];
                }
            }
        }
        System.out.println((System.nanoTime() - ns) + " ns");
        return result_array;
    }

}
