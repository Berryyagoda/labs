import org.junit.jupiter.api.Test;

public class FactorialTest {

    @Test
    public void factorialTest() {
        long result = Factorial.factorial(3);
        assert result == 6;
        result = Factorial.factorial(5);
        assert result == 120;
    }

    @Test
    public void factorialRecursiveTest() {
        long result = Factorial.factorialRecursive(3);
        assert result == 6;
        result = Factorial.factorialRecursive(5);
        assert result == 120;
    }

}
