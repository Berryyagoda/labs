public class Factorial {

    public static long factorial(int n) {
        long result = 1;
        for(int i = 2; i <= n; i++) {
            result *= i;
        }
        return result;
    }

    public static long factorialRecursive(int n) {
        if(n < 2)
            return 1;
        return factorialRecursive(n-1) * n;
    }

}
