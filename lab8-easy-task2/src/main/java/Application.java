public class Application {

    public static void main(String[] args) {
        System.out.println("Factorial(5): " + Factorial.factorial(5));
        System.out.println("Factorial(8) recursive: " + Factorial.factorialRecursive(8));
    }

}
