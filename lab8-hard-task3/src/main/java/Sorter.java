import java.util.Arrays;

public class Sorter {

    public static void sort(double[] array) {
        for(int i = 0; i < array.length - 1; i++) {
            for(int j = 1; j < array.length; j++) {
                if(array[j-1] > array[j]) {
                    double temp = array[j];
                    array[j] = array[j-1];
                    array[j-1] = temp;
                }
            }
        }
    }

    public static double[] createSortedArray(double[] array) {
        double[] newArray = Arrays.copyOf(array, array.length);
        sort(newArray);
        return newArray;
    }

}
