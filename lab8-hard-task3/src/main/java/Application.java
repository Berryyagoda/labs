public class Application {

    public static void main(String[] args) {
        double[] arr = {3, 9, 1, 0, 5};
        Sorter.sort(arr);
        for (double val : arr)
            System.out.print(val + " ");
        System.out.println();
        arr = new double[] {1, 6, 2, 3, 2};
        arr = Sorter.createSortedArray(arr);
        for (double val : arr)
            System.out.print(val + " ");
    }

}
