import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SorterTest {

    @Test
    public void sortTest() {
        double[] array = new double[] {4, 2, 7, 1, 5};
        Sorter.sort(array);
        Assertions.assertTrue(equals(new double[] {1, 2, 4, 5, 7}, array));
    }

    @Test
    public void createSortedTest() {
        double[] array = new double[] {4, 2, 7, 1, 5};
        array = Sorter.createSortedArray(array);
        Assertions.assertTrue(equals(new double[] {1, 2, 4, 5, 7}, array));
    }

    private boolean equals(double[] a, double[] b) {
        if(a.length != b.length)
            return false;
        for(int i = 0; i < a.length; i++) {
            if (a[i] != b[i])
                return false;
        }
        return true;
    }

}
