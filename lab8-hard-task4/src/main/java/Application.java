import java.util.Random;
import java.util.Scanner;

public class Application {

    private static final int N = 5000;

    public static void main(String[] args) throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        System.out.print("Введите порог: ");
        double limit = scanner.nextDouble();
        System.out.print("Введите коэффициент влияния порога: ");
        double coefficient = scanner.nextDouble();
        System.out.print("Введите максимальное время моделирования: ");
        int maxTime = scanner.nextInt();
        long startTime = System.currentTimeMillis();
        double minValue = -(limit*coefficient);
        double maxValue = limit*coefficient;
        double[][] buffer = new double[2][N];
        int i = 0;
        while(System.currentTimeMillis() - startTime < maxTime && i < buffer[0].length) {
            double value = minValue + (maxValue - minValue) * random.nextDouble();
            if(value > limit)
                break;
            else {
                buffer[0][i] = System.currentTimeMillis() - startTime;
                buffer[1][i] = value;
                i++;
            }
            Thread.sleep(random.nextInt(1000));
        }
        for (int j = 0; j < buffer[0].length; j++) {
            if(buffer[1][j] != 0 || buffer[0][j] != 0)
                System.out.println(buffer[0][j] + ":" + buffer[1][j]);
        }

    }

}
