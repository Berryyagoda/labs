import org.junit.jupiter.api.Test;

public class IntegerUtilsTest {

    @Test
    public void parseIntTest() {
        assert IntegerUtils.parseInt("123+123") == 246;
        assert IntegerUtils.parseInt("256-100+150") == 306;
        assert IntegerUtils.parseInt("-100") == -100;
        assert IntegerUtils.parseInt("-100+100") == 0;
    }

}
