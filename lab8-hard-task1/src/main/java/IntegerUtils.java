public class IntegerUtils {

    public static int parseInt(String input) {
        if(input.matches("^[0-9+-]+") && !input.isEmpty()) {
            String[] splitForSum = input.split("\\+");
            int finalResult = 0;
            for(String str : splitForSum) {
                String[] splitForDifference = str.split("-");
                int result = splitForDifference[0].isEmpty() ? 0 : Integer.parseInt(splitForDifference[0]);
                for(int i = 1; i < splitForDifference.length; i++)
                    result -= Integer.parseInt(splitForDifference[i]);
                finalResult += result;
            }
            return finalResult;
        }
        throw new NumberFormatException();
    }

}
