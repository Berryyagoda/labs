package task4;

public class Triangle extends Polygon {

    public Triangle(double line1, double line2, double line3) {
        super(new double[] { line1, line2, line3 });
    }

    public boolean isRectangular() {
        double a = lines[0]*lines[0];
        double b = lines[1]*lines[1];
        double c = lines[2]*lines[2];
        return a + b == c || a + c == b || b + c == a;
    }

    @Override
    public void print() {
        System.out.println("----------");
        System.out.println("Элемент: треугольник");
        System.out.println("Стороны треугольника:");
        System.out.println("a: " + lines[0]);
        System.out.println("b: " + lines[1]);
        System.out.println("c: " + lines[2]);
        System.out.println("Периметр: " + perimeter);
        System.out.println("----------");
    }
}
