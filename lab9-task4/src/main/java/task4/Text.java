package task4;

public class Text implements Element {

    private final String text;

    public Text(String text) {
        this.text = text;
    }

    @Override
    public void print() {
        System.out.println("----------");
        System.out.println("Элемент: текст");
        System.out.println("Значения элемента: " + text);
        System.out.println("----------");
    }
}
