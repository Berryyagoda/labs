package task4;

public class Application {

    public static void main(String[] args) {
        Polygon polygon = new Polygon(new double[] {3, 5, 7, 2});
        Triangle triangle1 = new Triangle(10, 5, 12);
        Triangle triangle2 = new Triangle(5, 7, 5);
        Triangle triangle3 = new Triangle(1, 2, 2);
        Text text1 = new Text("test text1");
        Text text2 = new Text("test text2");
        Operations.printElements(new Element[] {
                polygon,
                triangle1,
                triangle2,
                triangle3,
                text1,
                text2
        });
    }

}
