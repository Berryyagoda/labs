package task4;

public class Operations {

    public static void printElements(Element[] elements) {
        for (Element element : elements) {
            element.print();
        }
    }

}
