package task4;

public class Polygon implements Element {

    protected double[] lines;
    protected double perimeter;

    public Polygon(double[] lines) {
        this.lines = lines;
        if(!polygonCorrect())
            throw new IllegalArgumentException("Введены некорректные данные");
        perimeter = calculatePerimeter();
    }

    private double calculatePerimeter() {
        double result = 0;
        for (double line : lines) {
            result += line;
        }
        return result;
    }

    public double getPerimeter() {
        return perimeter;
    }

    private boolean polygonCorrect(){
        for (double line : this.lines) {
            if (line <= 0)
                return false;
            if (2 * line > perimeter)
                return false;
        }
        return true;
    }


    @Override
    public void print() {
        System.out.println("----------");
        System.out.println("Элемент: многоугольник");
        System.out.println("Стороны многоугольника:");
        for(int i = 0; i < lines.length; i++) {
            System.out.println("a" + (i+1) + ": " + lines[i]);
        }
        System.out.println("Периметр: " + perimeter);
        System.out.println("----------");
    }

}
