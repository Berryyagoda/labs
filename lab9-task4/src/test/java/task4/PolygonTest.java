package task4;

import org.junit.jupiter.api.Test;
import task4.Polygon;
import task4.Triangle;

import static org.junit.jupiter.api.Assertions.*;

public class PolygonTest {

    @Test
    public void perimeterTest() {
        Polygon polygon = new Triangle(10, 5, 8);
        assertEquals(23, polygon.getPerimeter());
    }

    @Test
    public void isRectangularTest() {
        Triangle triangle = new Triangle(6, 8, 10);
        assertTrue(triangle.isRectangular());
    }

    @Test
    public void correctPolygonTest() {
        Polygon polygon = new Polygon(new double[] {50, 1, 2, 4});
        assertFalse(polygon.polygonCorrect());
    }

}
